//gulp include
var gulp = require('gulp');
//include plugin
var gutil   = require('gulp-util'),
    sass    = require('gulp-sass'),
    uglify  = require('gulp-uglify'),
    concat  = require('gulp-concat'),
    browserSync = require('browser-sync').create();





gulp.task('log', function() {
  gutil.log('== error cuyy gulp ==');
});



gulp.task('sass', function() {
  return gulp.src('src/sass/custom/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .on('error', gutil.log)
        .pipe(concat('custom.min.css'))
        .pipe(gulp.dest('build/css'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('serve', function() {

    browserSync.init({
        server: {
            baseDir: "./build"
        }
    });

    gulp.watch("./src/sass/custom/**/*.scss", ['sass']);
    gulp.watch("./build/**/*.html").on('change', browserSync.reload);
});


gulp.task('js', function() {
   return gulp.src(['src/js/jquery/jquery-2.1.1.js',
                    'src/js/moment/moment.2.9.0.js',
                    'src/js/moment/moment-with-locales.min.js',
                    //'src/js/bootstrap/bootstrap.3.1.1.min.js',
                    //'src/js/bootstrap/bootstrap-3.2.min.js',
                    'src/js/bootstrap/bootstrap.min.js',
                    'src/js/bootstrap/datepicker/bootstrap-datetimepicker.js',
                    //'src/js/bootstrap/datepicker/bootstrap-datetimepicker.js',
          /*blueimp*/'src/js/blueimp/blueimp-gallery.min.js',
                     'src/js/smoothscroll.js',
                      'src/js/mobile-menu.js',
                      'src/js/elevatezoom/jquery.elevatezoom.js',
                        /*ratting pakai rateyo*/
                        'src/js/rateyo/jquery.rateyo.min.js',
                        'src/js/rateyo/rateyo.custom.js',
                        /*ratting pakai rateyo end*/
                        'src/js/flexSlider/jquery.flexslider-min.js',
                        'src/js/_lightslider/lightslider.js',
                        'src/js/photoswipe/photoswipe.min.js',
                        'src/js/photoswipe/photoswipe-ui-default.min.js',
                          'src/js/modalvideobantuan/modal-video.js',
                        'src/js/Select2_k/select2.full.min.js',
                        'src/js/Slick_k/slick.min.js',

                          'src/js/scripts.js'])
   .pipe(concat('custom.js'))
   .pipe(gulp.dest('build/js'))
   .pipe(concat('custom.min.js'))
   .pipe(uglify())
   .pipe(gulp.dest('build/js'));
});




//gulp.task('watch', ['browser-sync'], function () {
  //  gulp.watch("scss/*.scss", ['sass']);
  //  gulp.watch("*.html").on('change', browserSync.reload);
//});

gulp.task('default', ['sass','serve']);
