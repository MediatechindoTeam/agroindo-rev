/// <reference path="../jquery.min.js" />

"use strict";

function ModalbantuanVideoOptions(callbackOnModalbantuanOpenClick) {
    if (this instanceof ModalbantuanVideoOptions) {

        this.callbackOnModalbantuanOpenClick = callbackOnModalbantuanOpenClick;
        this._videoType = undefined;

        if (Object.freeze)
            Object.freeze(this.VideoEnum);
    }

    else {
        return new ModalbantuanVideoOptions(callbackOnModalbantuanOpenClick);
    }
}

ModalbantuanVideoOptions.prototype = {
    constructor: ModalbantuanVideoOptions,


    getVideoType : function () {
        return this._videoType;
    },

    setVideoType : function (value) {
        if (typeof value != "number") {
            throw new Error('Invalid argument: value. This argument must be a number.');
        }

        var videoTypeItem;
        for(videoTypeItem in this.VideoEnum) {
            var enumValue = this.VideoEnum[videoTypeItem];

            if (value == enumValue) {
                this._videoType = enumValue;
            }
        }

        if (this._videoType == undefined)
            throw new Error('Invalid argument: value. This argument must be in the range of VideoEnum.');
    },


    VideoEnum : {
        MP4: 0,
        YOUTUBE: 1
    }
};

function ytVidId(url) {
    var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    return (url.match(p)) ? RegExp.$1 : false;
}

(function ($) {

    function showModalbantuan(options) {

        if ($('#mask').length == 0) {
            $('body').prepend("<div id='mask' class='modalbantuan-backdrop fade in'></div>");
        }

        var dialogContainer = $('#dialog-container-video');

        if (dialogContainer.length == 0) {

            var dialogContainer = $("<div id='dialog-container-video' class='modalbantuan fade in'></div>");
            $('body').prepend(dialogContainer);

        }

        var dialogContent;

        if (options.getVideoType() == options.VideoEnum.MP4)
            dialogContent = "<div id='dialog-content' class='modalbantuan-body'><video width='100%' src='" + options.link + "' controls></video></div>";

        else if (options.getVideoType() == options.VideoEnum.YOUTUBE)
            dialogContent = "<div id='dialog-content' class='modalbantuan-body modalbantuan-body-video'><iframe src='https://www.youtube.com/embed/" + options.youtubeId + "' frameborder='0' allowfullscreen></iframe></div>";

        if(dialogContent)
            dialogContainer.append(dialogContent);

        $('#mask').show();
        $("#dialog-container-video").show();

        $('body').css('overflow', 'hidden');

        var closePopup = function () {
            $('#mask').hide();
            $('#dialog-container-video').hide();

            $("#mask").unbind("click", closePopup);
            $('#dialog-container-video a.close').unbind("click", closePopup);
            $(document).unbind("keyup", escKeyClosePopup);

            $('#dialog-content').remove();

            $('body').css('overflow', 'auto');
        };

        $('#mask').on("click", closePopup);
        $('#dialog-container-video a.close').on("click", closePopup);

        var escKeyClosePopup = function (e) {
            if (e.keyCode == 27) {
                closePopup();
            }
        };

        $(document).keyup(escKeyClosePopup);
    }

    $.fn.modalbantuanvideo = function (options) {

        if ((options instanceof ModalbantuanVideoOptions) == false)
            throw new Error('Invalid argument: options. This argument must be an instance of ModalbantuanVideoOptions.');

        if (!options || $.isEmptyObject(options)) {
            options = new ModalbantuanVideoOptions();
        }

        $(this).each(function (i) {

            var link = $(this).prop("href");

            if (typeof link == "undefined") {
                return true;
            }

            var youtubeId = ytVidId(link);

            if (youtubeId != false) {
                options.setVideoType(options.VideoEnum.YOUTUBE);
                options.youtubeId = youtubeId;
            }

            else if (link.indexOf('.mp4', link.length - '.mp4'.length) != -1) {
                options.setVideoType(options.VideoEnum.MP4);
            }

            else
                return true;

            options.link = typeof options.link == "undefined" ? link : options.link;

            $(this).click(function (e) {
                e.preventDefault();

                showModalbantuan(options);


                if (typeof options.callbackOnModalbantuanOpenClick == "function") {
                    options.callbackOnModalbantuanOpenClick();
                }
            });

        });
    }
})(jQuery);
